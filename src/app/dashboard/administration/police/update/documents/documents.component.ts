import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-police-documents',
  templateUrl: './documents.component.html',
  styleUrls: []
})
export class DocumentsPoliceComponent implements OnInit {
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void { }

}
