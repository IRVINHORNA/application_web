module.exports = {
  getPolices: {
    "page": {
      "totalNumberOfItems": 128,
      "numberOfPages": 10
    },
    "polices": [{
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
        "cip": "12345",
        "names": "Brayan Christian",
        "lastNames": "Reyes Gamarra",
        "document": {
          "code": "DNI",
          "number": "76543211"
        }
      },
      {
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12",
        "cip": "3456",
        "names": "Jesus",
        "lastNames": "Llanos Torres",
        "document": {
          "code": "DNI",
          "number": "44454555"
        }
      },
      {
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12",
        "cip": "23456",
        "names": "Carlos Vidal",
        "lastNames": "Roca Bejar",
        "document": {
          "code": "DNI",
          "number": "78900987"
        }
      },
      {
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12",
        "cip": "23456",
        "names": "Carlos Vidal",
        "lastNames": "Roca Bejar",
        "document": {
          "code": "DNI",
          "number": "78900987"
        }
      },
      {
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12",
        "cip": "23456",
        "names": "Carlos Vidal",
        "lastNames": "Roca Bejar",
        "document": {
          "code": "DNI",
          "number": "78900987"
        }
      }
    ]
  },
  createPolice: {
    "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
    "cip": "12345",
    "division": {
      "id": "1",
      "name": "División de Carabayllo"
    },
    "firstName": "Brayan",
    "secondName": "Christian",
    "lastName": "Reyes",
    "secondLastName": "Gamarra",
    "position": {
      "id": "1",
      "name": "Jefatura"
    },
    "grade": {
      "id": "1",
      "name": "Mayor"
    },
    "sex": {
      "id": "1",
      "name": "Hombre"
    },
    "birthOfDate": "31/03/1993",
    "pseudonym": "Caracortada",
    "civilStatus": {
      "id": "1",
      "name": "Soltero"
    }
  },
  policeGeneralInformationDetail: {
    "id": "67ad327e-7e4b-463b-b6de-200c9c9fe530",
    "cip": "9876543212",
    "division": {
      "id": "67ad327e-7e4b-463b-b6de-200c9c9fe530",
      "name": "DIVISION LOS OLIVOS"
    },
    "firstName": "MANOLO",
    "secondName": "MANUEL",
    "lastName": "REYES",
    "secondLastName": "REYNOSO",
    "position": {
      "id": 1,
      "name": "Nombre del parámetro"
    },
    "grade": {
      "id": 1,
      "name": "Nombre del parámetro"
    },
    "sex": {
      "id": 1,
      "name": "Nombre del parámetro"
    },
    "dateOfBirth": "31/03/1993",
    "pseudonym": "AGUILA",
    "civilStatus": {
      "id": 1,
      "name": "Nombre del parámetro"
    }
  },
  updatePolice: {
    "id": "67ad327e-7e4b-463b-b6de-200c9c9fe530",
    "cip": "9876543212",
    "division": {
      "id": "67ad327e-7e4b-463b-b6de-200c9c9fe530",
      "name": "DIVISION LOS OLIVOS"
    },
    "firstName": "MANOLO",
    "secondName": "MANUEL",
    "lastName": "REYES",
    "secondLastName": "REYNOSO",
    "position": {
      "id": 1,
      "name": "Nombre del parámetro"
    },
    "grade": {
      "id": 1,
      "name": "Nombre del parámetro"
    },
    "sex": {
      "id": 1,
      "name": "Nombre del parámetro"
    },
    "dateOfBirth": "31/03/1993",
    "pseudonym": "AGUILA",
    "civilStatus": {
      "id": 1,
      "name": "Nombre del parámetro"
    }
  }
}
