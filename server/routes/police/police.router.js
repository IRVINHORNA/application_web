const express = require('express');
const router = express.Router();
const { getPolices, createPolice, policeGeneralInformationDetail, updatePolice } = require('./police.data');
const delayTime = 1000;

/* =========== GET POLICES ============ */
router.get('/police-management/v1/polices', (req, res) => {
    let status = 200;
    data = getPolices;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== CREATE POLICE ============ */
router.post('/police-management/v1/polices/general-information', (req, res) => {
    let status = 200;
    data = createPolice;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== GET GENERAL INFORMATION POLICE BY ID ============ */
router.get('/police-management/v1/polices/general-information/:id', (req, res) => {
    let status = 200;
    data = policeGeneralInformationDetail;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== UPDATE POLICE BY ID ============ */
router.put('/police-management/v1/polices/general-information/:id', (req, res) => {
    let status = 200;
    data = updatePolice;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});


/* =========== DELETE POLICE BY ID ============ */
router.delete('/police-management/v1/polices/:id', (req, res) => {
    let status = 200;
    data = {};
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

module.exports = router;