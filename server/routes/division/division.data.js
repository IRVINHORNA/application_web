module.exports = {
  getDivisions: {
    "page": {
      "totalNumberOfItems": 128,
      "numberOfPages": 10
    },
    "divisions": [{
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
        "code": "DIV01",
        "acronym": "DIVLO",
        "name": "División de Los Olivos",
        "description": "División con más de 10 años...",
        "address": {
          "id": 1,
          "name": "Los Tulipanes",
          "number": "424",
          "typeId": 1,
          "districtId": 1,
          "provinceId": 1,
          "departmentId": 1
        },
        "contact": {
          "email": "example@abc.com",
          "phoneNumber": "55555",
          "annexNumber": "555"
        }
      },
      {
        "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12",
        "code": "DIV02",
        "acronym": "DIVPL",
        "name": "División de San MArtin de Porres",
        "description": "División con más de 10 años...",
        "address": {
          "id": 1,
          "name": "Los Tulipanes",
          "number": "424",
          "typeId": 1,
          "districtId": 1,
          "provinceId": 1,
          "departmentId": 1
        },
        "contact": {
          "email": "example@abc.com",
          "phoneNumber": "55555",
          "annexNumber": "555"
        }
      }
    ]
  },
  createDivision: {},
  updateDivision: {
    "id": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
    "code": "DIV01",
    "acronym": "DIVLO",
    "name": "División de Los Olivos",
    "description": "División con más de 10 años...",
    "address": {
      "id": 1,
      "name": "Los Tulipanes",
      "number": "424",
      "typeId": 1,
      "districtId": 1,
      "provinceId": 1,
      "departmentId": 1
    },
    "contact": {
      "email": "example@abc.com",
      "phoneNumber": "55555",
      "annexNumber": "555"
    }
  },
  divisionDetail: {
    "id": 5,
    "code": "DV01",
    "acronym": "DV",
    "name": "name",
    "description": "Description description description",
    "address": {
      "name": "calle calle",
      "number": "523",
      "type": {
        "id": 1,
        "name": "Avenida"
      },
      "district": {
        "id": 1,
        "name": "Olivos"
      },
      "province": {
        "id": 1,
        "name": "Lima"
      },
      "department": {
        "id": 1,
        "name": "Lima"
      }
    },
    "contact": {
      "email": "example@abc.com",
      "phoneNumber": "555555",
      "annexNumber": "5555"
    }

  }
}
