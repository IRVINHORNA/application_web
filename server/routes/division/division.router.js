const express = require('express');
const router = express.Router();
const { getDivisions, createDivision, divisionDetail, updateDivision } = require('./division.data');
const delayTime = 1000;

/* =========== GET DIVISIONS ============ */
router.get('/administration-management/v1/divisions', (req, res) => {
    let status = 200;
    data = getDivisions;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== CREATE DIVISION ============ */
router.post('/administration-management/v1/divisions', (req, res) => {
    let status = 200;
    data = createDivision;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== GET DIVISION BY ID ============ */
router.get('/administration-management/v1/divisions/:id', (req, res) => {
    let status = 200;
    data = divisionDetail;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== UPDATE DIVISION BY ID ============ */
router.put('/administration-management/v1/divisions/:id', (req, res) => {
    let status = 200;
    data = updateDivision;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});


/* =========== DELETE DIVISION BY ID ============ */
router.delete('/administration-management/v1/divisions/:id', (req, res) => {
    let status = 200;
    data = {};
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

module.exports = router;