const express = require('express');
const router = express.Router();
const { getDepartments, getProvinces, getDistricts } = require('./ubigeo.data');
const delayTime = 1000;

/* =========== GET DEPARTMENTS ============ */
router.get('/parameter-management/v1/ubigeos/departments', (req, res) => {
    let status = 200;
    data = getDepartments;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== GET PROVINCES ============ */
router.get('/parameter-management/v1/ubigeos/provinces', (req, res) => {
    let status = 200;
    data = getProvinces;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

/* =========== GET DISTRICS ============ */
router.get('/parameter-management/v1/ubigeos/districts', (req, res) => {
    let status = 200;
    data = getDistricts;
    console.log(data);
    setTimeout(() => res.status(status).send(data), delayTime);
});

module.exports = router;